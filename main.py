import gym
# import keras
import tensorflow.keras as keras
import numpy as np
import random
from collections import deque
# keras = tf.keras

class DQNAgent:
    def __init__(self, state_size, action_size):
        self.state_size = state_size
        self.action_size = action_size
        self.memory = deque(maxlen=2000)
        self.gamma = 0.95    # discount rate
        self.epsilon = 1.0  # exploration rate
        self.epsilon_min = 0.01
        self.epsilon_decay = 0.995
        self.learning_rate = 0.001
        self.model = self._build_model()

    def _build_model(self):
        # Neural Net for Deep-Q learning Model
        model = keras.models.Sequential([
            keras.layers.Dense(16, input_dim=self.state_size, activation='relu'),
            keras.layers.Dense(32, activation='relu'),
            keras.layers.Dense(self.action_size, activation='linear')
        ])
        model.compile(loss='mse',
                      optimizer=keras.optimizers.Adam(lr=self.learning_rate))
        model.summary()
        return model

    def remember(self, state, action, reward, next_state, done):
        self.memory.append((state, action, reward, next_state, done))

    def act(self, state):
        if np.random.rand() <= self.epsilon:
            return random.randrange(self.action_size)
        act_values = self.model.predict(state)
        return np.argmax(act_values[0])  # returns action

    def replay(self, batch_size):
        minibatch = random.sample(self.memory, batch_size if batch_size <= len(self.memory) else len(self.memory))
        for state, action, reward, next_state, done in minibatch:
            target = reward
            if not done:
                target = reward + self.gamma * \
                         np.amax(self.model.predict(next_state)[0])
            target_f = self.model.predict(state)
            target_f[0][action] = target
            self.model.fit(state, target_f, epochs=1, verbose=0)
        if self.epsilon > self.epsilon_min:
            self.epsilon *= self.epsilon_decay


if __name__ == "__main__":
    env = gym.make('CartPole-v0')
    agent = DQNAgent(4, 2)
    for e in range(10000):
        state = env.reset()
        state = np.reshape(state, [1, 4])
        render = e % 100 == 0
        time_t = 0
        while True:
            if render:
                env.render()
            action = agent.act(state)
            next_state, reward, done, _ = env.step(action)
            reward = -1 if done else 1
            next_state = np.reshape(next_state, [1, 4])
            agent.remember(state, action, reward, next_state, done)
            state = next_state
            time_t += 1
            if done:
                # print the score and break out of the loop
                print("episode: {}/{}, score: {}"
                      .format(e, 10000, time_t))
                break
        # train the agent with the experience of the episode
        agent.replay(64)
